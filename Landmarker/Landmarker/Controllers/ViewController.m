//
//  ViewController.m
//  Landmarker
//
//  Created by Jason Lagaac on 24/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "LandmarkStoreController.h"
#import "Landmark.h"

@interface ViewController (PrivateMethods)
- (NSString *)determineSettings;
@end


@implementation ViewController

@synthesize mapView, landmarkView;
@synthesize favouritesButton, categoryButton, periodButton;
@synthesize periodMenuView, categoryMenuView, landmarkViewHeader, landmarkViewText;
@synthesize commercialSettingButton, civicSettingButton, residentialSettingButton;
@synthesize religiousSettingButton, museumSettingButton, otherSettingButton;
@synthesize period1600Button, period1700Button, period1800Button, period1900Button;
@synthesize landmarkFavouriteButton, landmarkDivider, landmarkShadowLayer, landmarkImageButton;
@synthesize landmarkImageView, attribLabel;

#pragma mark - Initialisation and Deallocation Functions

- (id)init 
{
    
    if (self = [super init]) 
    {
        landmarkView.layer.zPosition = -1;
        currPredicateStatement = @"all";
    }
    
    return self;
}

- (void)dealloc 
{
    [locationManager release];
    [mapView release];
    
    locationManager = nil;
    mapView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"LandmarkInfoView" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"PeriodMenuView" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"CategorySelectView" owner:self options:nil];
    
    landmarkStoreController = [[LandmarkStoreController alloc] init];
    
    // Setup location manager
    locationManager = [[CLLocationManager alloc] init];
    
    // Set defaults to within 500m
    [locationManager setDelegate:self];
    [locationManager setDistanceFilter:kCLLocationAccuracyKilometer];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
    
    // Setup the MapView and set delegates
    [mapView setShowsUserLocation:YES];
    [mapView setDelegate:self];

    // Start the location manager
    [locationManager startUpdatingLocation];
    NSLog(@"zPosition: %f", [[mapView layer] zPosition]);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    landmarks = [landmarkStoreController getAllLandmarks];
    [landmarks retain];
    
    firstRun = YES;
    [mapView addAnnotations:landmarks];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Map View Delegates
////////////////////////////////////////////////////////////////////////////////
- (void)mapView:(MKMapView *)mv didUpdateUserLocation:(MKUserLocation *)u
{
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([u location].coordinate, 1000, 1000); 
        [mapView setRegion:region animated:YES];
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[Landmark class]]) {
        
        MKAnnotationView *annotationView = 
        (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] 
                              initWithAnnotation:annotation 
                              reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        [annotationView setImage:[UIImage imageNamed:@"PinEx@2x.png"]];
        
        
        // Create a UIButton object to add on the 
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setTitle:annotation.title forState:UIControlStateNormal];
        [annotationView setRightCalloutAccessoryView:rightButton];
        
        return annotationView;
    }
    
    return nil; 
}

- (void)mapView:(MKMapView *)mv
 annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control 
{
    Landmark *tmpItem = [view annotation];
    
    if ([(UIButton*)control buttonType] == UIButtonTypeDetailDisclosure){
        // Do your thing when the detailDisclosureButton is touched
        
        
        [landmarkView setCenter:CGPointMake(160, 252)];
        [landmarkViewHeader setText:[tmpItem title]];
        
        if ([[tmpItem title] length] > 20 && [[tmpItem title] length] < 25)
        {
            [landmarkViewHeader setFont:[UIFont fontWithName:@"YanoneKaffeesatz-Bold" size:28.0f]];
        }
        else if ([[tmpItem title] length] < 20) 
        {
            [landmarkViewHeader setFont:[UIFont fontWithName:@"YanoneKaffeesatz-Bold" size:33.0f]];
        } 
        else 
        {
            [landmarkViewHeader setFont:[UIFont fontWithName:@"YanoneKaffeesatz-Bold" size:25.0f]];
        }
        
        [landmarkViewText setText:[tmpItem info]];
        [landmarkViewText setFont:[UIFont fontWithName:@"YanoneKaffeesatz-Regular" size:18.0f]];
        [landmarkViewHeader setLineBreakMode:UILineBreakModeWordWrap];
        landmarkViewHeader.numberOfLines = 0;
        [landmarkFavouriteButton setSelected:[tmpItem favourite]];
        presentedLandmark = tmpItem;
        
        if ([tmpItem owner])
        {
            NSString *attribution = [NSString stringWithFormat:@"Photo by %@, used under a Creative Commons Attribution-Unported (3.0) license", [tmpItem owner]];
            NSLog(@"%@", [tmpItem owner]);
            [attribLabel setText:attribution];
        } else {
            [attribLabel setText:@""];
        }

        
        NSString *filename = [NSString stringWithString:[tmpItem title]];
        filename = [filename stringByReplacingOccurrencesOfString:@"&" withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@"-" withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@" " withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@"'" withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@"," withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@"." withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@"(" withString:@""];
        filename = [filename stringByReplacingOccurrencesOfString:@")" withString:@""];

        filename = [[filename lowercaseString] stringByAppendingPathExtension:@"jpeg"];
        
        [landmarkImageView setImage:[UIImage imageNamed:filename]];
        
        [self presentLandmarkView];
    }
}


- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views 
{ 
    for (MKAnnotationView *aV in views) {
        CGRect endFrame = aV.frame;
        
        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - 230.0, aV.frame.size.width, aV.frame.size.height);
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.45];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [aV setFrame:endFrame];
        [UIView commitAnimations];
    }
    
    [mapView setShowsUserLocation:YES];
}

#pragma mark - Location Manager Delegates
////////////////////////////////////////////////////////////////////////////////
- (void)locationManager:(CLLocationManager *)manager 
    didUpdateToLocation:(CLLocation *)newLocation 
           fromLocation:(CLLocation *)oldLocation  {
    
    if(!firstRun) {
        return;
    }
    
    if (currLoc.latitude != 0.0f && currLoc.longitude != 0.0f) 
    {
        firstRun = NO;
        currLoc = [newLocation coordinate];
        [mapView setCenterCoordinate:currLoc];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Cannot Determine Location"
                                                    message: @"Please connect to Wi-Fi or check your data settings"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}


#pragma mark - Landmark View Operations
////////////////////////////////////////////////////////////////////////////////

- (IBAction)closeLandmarkView:(id)sender
{
    
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationTransitionNone
                     animations:^{
                         [landmarkView setAlpha:0.0];
                     } 
                     completion:^(BOOL finished){
                         [landmarkView removeFromSuperview];
                         
                         [categoryButton setHidden:NO];
                         [periodButton setHidden:NO];
                         [favouritesButton setHidden:NO];
                         
                         [UIView animateWithDuration:0.2
                                               delay:0.0
                                             options: UIViewAnimationTransitionNone
                                          animations:^{
                                              [categoryButton setCenter:CGPointMake(categoryButton.center.x, 69)];
                                              [favouritesButton setCenter:CGPointMake(favouritesButton.center.x, 69)];
                                              [periodButton setCenter:CGPointMake(periodButton.center.x, 69)];
                                              
                                              if ([landmarkFavouriteButton isSelected]) 
                                                  [presentedLandmark setFavourite:YES];
                                              else
                                                  [presentedLandmark setFavourite:NO];
                                              
                                              
                                              [landmarkStoreController updateLandmarkFavourte:presentedLandmark];
                                          } 
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:0.2
                                                                    delay:0.0
                                                                  options: UIViewAnimationTransitionNone
                                                               animations:^{
                                                                   [landmarkDivider setAlpha:1.0f];
                                                                   [landmarkViewText setAlpha:1.0f];
                                                                   [landmarkShadowLayer setAlpha:1.0f];
                                                                   [landmarkViewHeader setAlpha:1.0f];
                                                                   [landmarkImageButton setSelected:NO];
                                                               } 
                                                               completion:nil];
                                              
                                          }];
                     }];
}

- (void)presentLandmarkView
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationTransitionNone
                     animations:^{
                         [categoryButton setCenter:CGPointMake(categoryButton.center.x, 75)];
                         [favouritesButton setCenter:CGPointMake(favouritesButton.center.x, 75)];
                         [periodButton setCenter:CGPointMake(periodButton.center.x, 75)];
                     } 
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5
                                               delay:0.0
                                             options: UIViewAnimationTransitionNone
                                          animations:^{
                                              [categoryButton setCenter:CGPointMake(categoryButton.center.x, 30)];
                                              [favouritesButton setCenter:CGPointMake(favouritesButton.center.x, 30)];
                                              [periodButton setCenter:CGPointMake(periodButton.center.x, 30)];
                                              
                                              
                                          } 
                                          completion:^(BOOL finished){
                                              [categoryButton setHidden:YES];
                                              [periodButton setHidden:YES];
                                              [favouritesButton setHidden:YES];
                                              
                                              
                                              [landmarkView setAlpha:0.0];
                                              [[self view] addSubview:landmarkView];
                                              
                                              [UIView animateWithDuration:0.2
                                                                    delay:0.0
                                                                  options: UIViewAnimationTransitionNone
                                                               animations:^{
                                                                   [landmarkView setAlpha:1.0];
                                                                   [landmarkImageView setAlpha:0.3];
                                                                   [attribLabel setAlpha:0.0f];
                                                               } 
                                                               completion:nil];
                                          }];
                     }];
}

#pragma mark - Period Menu Operations
////////////////////////////////////////////////////////////////////////////////
- (IBAction)presentPeriodMenu:(id)sender
{
    if (![periodButton isSelected]) {
        [periodMenuView setCenter:CGPointMake(160, 265)];
        
        [periodMenuView setAlpha:0.0f];
        [[self view] addSubview:periodMenuView];
        [periodButton setSelected:YES];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [categoryButton setUserInteractionEnabled:NO];
                             [favouritesButton setUserInteractionEnabled:NO];
                             
                             [categoryButton setAlpha:0.6f];
                             [favouritesButton setAlpha:0.6f];
                             
                             [periodButton setCenter:CGPointMake(periodButton.center.x, 75)];
                             [periodMenuView setAlpha:1.0f];
                         } 
                         completion:nil];
    } else {
        [periodButton setSelected:NO];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [periodButton setCenter:CGPointMake(periodButton.center.x, 69)];
                             [periodMenuView setAlpha:0.0f];
                             
                             [categoryButton setAlpha:1.0f];
                             [favouritesButton setAlpha:1.0f];
                             
                         } 
                         completion:^(BOOL finished){ 
                             [categoryButton setUserInteractionEnabled:YES];
                             [favouritesButton setUserInteractionEnabled:YES];
                             [periodMenuView removeFromSuperview];
                             
                             NSString *statement = [self determineSettings];
                             
                             if ([statement isEqualToString:@"all"])
                             {
                                 [mapView removeAnnotations:[mapView annotations]];
                                 [mapView addAnnotations:landmarks];
                             }
                             else if ([statement length])
                             {
                                 NSPredicate *predicate = [NSPredicate predicateWithFormat:statement];
                                 [mapView removeAnnotations:[mapView annotations]];
                                 [mapView addAnnotations:[landmarks filteredArrayUsingPredicate:predicate]];
                             }
                         }];
    }
    
}


#pragma mark - Category Menu Operations
////////////////////////////////////////////////////////////////////////////////
- (IBAction)presentCategoryMenu:(id)sender
{
    if (![categoryButton isSelected]) {
        [categoryMenuView setCenter:CGPointMake(160, 270)];
        
        [categoryMenuView setAlpha:0.0f];
        [[self view] addSubview:categoryMenuView];
        [categoryButton setSelected:YES];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [periodButton setUserInteractionEnabled:NO];
                             [favouritesButton setUserInteractionEnabled:NO];
                             
                             [periodButton setAlpha:0.6f];
                             [favouritesButton setAlpha:0.6f];
                             
                             [categoryButton setCenter:CGPointMake(categoryButton.center.x, 75)];
                             [categoryMenuView setAlpha:1.0f];
                         } 
                         completion:nil];
    } else {
        [categoryButton setSelected:NO];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [categoryButton setCenter:CGPointMake(categoryButton.center.x, 69)];
                             [categoryMenuView setAlpha:0.0f];
                             
                             [periodButton setAlpha:1.0f];
                             [favouritesButton setAlpha:1.0f];
                         } 
                         completion:^(BOOL finished){ 
                             [categoryMenuView removeFromSuperview];
                             
                             [periodButton setUserInteractionEnabled:YES];
                             [favouritesButton setUserInteractionEnabled:YES];
                             
                             NSString *statement = [self determineSettings];
                             NSLog(@"%@", statement);
                             
                             if ([statement isEqualToString:@"all"])
                             {
                                 [mapView removeAnnotations:[mapView annotations]];
                                 [mapView addAnnotations:landmarks];
                             }
                             else if ([statement length])
                             {
                                 NSPredicate *predicate = [NSPredicate predicateWithFormat:statement];
                                 [mapView removeAnnotations:[mapView annotations]];
                                 [mapView addAnnotations:[landmarks filteredArrayUsingPredicate:predicate]];
                             }
                         }];
    }
    
}

- (IBAction)toggleButton:(id)sender
{
    UIButton *btn = sender;
    [btn setSelected:![btn isSelected]];
    NSLog(@"%@",[btn currentTitle]);
}


- (NSString *)determineSettings
{
    NSMutableString *categoryStatement = [[NSMutableString alloc] init];
    NSMutableString *periodStatement = [[NSMutableString alloc] init];
    
    NSMutableString *predicateStatement = [[NSMutableString alloc] init];
    
    NSMutableArray *categoryFlags = [[NSMutableArray alloc] init];
    NSMutableArray *periodFlags = [[NSMutableArray alloc] init];
    
    // Determine category flags
    if ([commercialSettingButton isSelected]) 
    {
        [categoryFlags addObject:[NSNumber numberWithInt:0]];
    } 
    
    if ([civicSettingButton isSelected])
    {
        [categoryFlags addObject:[NSNumber numberWithInt:1]];
    }
    
    if ([museumSettingButton isSelected])
    {
        [categoryFlags addObject:[NSNumber numberWithInt:2]];
    }
    
    if ([religiousSettingButton isSelected])
    {
        [categoryFlags addObject:[NSNumber numberWithInt:3]];
    }
    
    if ([residentialSettingButton isSelected])
    {
        [categoryFlags addObject:[NSNumber numberWithInt:4]];
    }
    
    if ([otherSettingButton isSelected])
    {
        [categoryFlags addObject:[NSNumber numberWithInt:5]];
    }
    
    
    for (int i = 0; i < [categoryFlags count]; i++)
    {
        if (i == 0)
            [categoryStatement appendString:[NSString stringWithFormat:@"(type = %d) ", [[categoryFlags objectAtIndex:i] intValue]]];
        else 
            [categoryStatement appendString:[NSString stringWithFormat:@"OR (type = %d) ", [[categoryFlags objectAtIndex:i] intValue]]];
    }
    
    if ([categoryStatement length])
    {
        [categoryStatement insertString:@"(" atIndex:0];
        [categoryStatement insertString:@")" atIndex:([categoryStatement length]-1)];
    }
    
    
    // Determine period flags
    if ([period1600Button isSelected])
    {
        [periodFlags addObject:[NSNumber numberWithInt:1600]];
    }
    
    if ([period1700Button isSelected])
    {
        [periodFlags addObject:[NSNumber numberWithInt:1700]];
    }
    
    if ([period1800Button isSelected])
    {
        [periodFlags addObject:[NSNumber numberWithInt:1800]];
    }
    
    if ([period1900Button isSelected])
    {
        [periodFlags addObject:[NSNumber numberWithInt:1900]];
    }
    
    
    for (int i = 0; i < [periodFlags count]; i++)
    {
        if (i == 0)
            [periodStatement appendString:[NSString stringWithFormat:@"(builtDate = %d) ", [[periodFlags objectAtIndex:i] intValue]]];
        else 
            [periodStatement appendString:[NSString stringWithFormat:@"OR (builtDate = %d) ", [[periodFlags objectAtIndex:i] intValue]]];
    }
    
    if ([periodStatement length])
    {
        [periodStatement insertString:@"(" atIndex:0];
        [periodStatement insertString:@")" atIndex:([periodStatement length]-1)];
    }
    
    if ([periodStatement length] && [categoryStatement length])
    {
        [predicateStatement appendString:periodStatement];
        [predicateStatement appendString:@" AND "];
        [predicateStatement appendString:categoryStatement];
        
        
    }
    else if ([categoryStatement length])
    {
        [predicateStatement appendString:categoryStatement];
    }
    else if ([periodStatement length])
    {
        [predicateStatement appendString:periodStatement];
    }
    
    if ([periodStatement length]) 
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [periodButton setImage:[UIImage imageNamed:@"PeriodSelected.png"] 
                                           forState:UIControlStateNormal];
                             
                         } 
                         completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [periodButton setImage:[UIImage imageNamed:@"Periods.png"] 
                                           forState:UIControlStateNormal];
                             
                         } 
                         completion:nil];
    }
    
    if ([categoryStatement length]) 
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [categoryButton setImage:[UIImage imageNamed:@"CategoriesSelected.png"] 
                                             forState:UIControlStateNormal];
                             
                         } 
                         completion:nil];
    }
    else 
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [categoryButton setImage:[UIImage imageNamed:@"Categories.png"] 
                                             forState:UIControlStateNormal];
                             
                         } 
                         completion:nil];
    }
    
    
    
    
    if ([predicateStatement length]) 
    {
        if ([favouritesButton isSelected])
        {
            [predicateStatement appendString:@" AND (favourite = 1)"];
        }
        
        if (![currPredicateStatement isEqualToString:predicateStatement]) 
        {
            currPredicateStatement = predicateStatement;
            return currPredicateStatement;
        } 
        else
        {
            return nil;
        }
    }
    else 
    {
        if (![currPredicateStatement isEqualToString:@"all"] || ![currPredicateStatement isEqualToString:@"(favourite = 1)"])
        {
            if ([favouritesButton isSelected])
            {
                currPredicateStatement = @"(favourite = 1)";
            }
            else 
            {
                currPredicateStatement = @"all";
            }
            
            return currPredicateStatement;
        } 
        else
        {
            return nil;
        }
    }
}

- (IBAction)toggleImageView:(id)sender
{
    if (![landmarkImageButton isSelected])
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [landmarkDivider setAlpha:0.0f];
                             [landmarkViewText setAlpha:0.0f];
                             [landmarkShadowLayer setAlpha:0.0f];
                             [landmarkViewHeader setAlpha:0.0f];
                             [landmarkImageView setAlpha:1.0f];
                             [attribLabel setAlpha:1.0f];
                             [landmarkImageButton setSelected:YES];
                         } 
                         completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationTransitionNone
                         animations:^{
                             [landmarkDivider setAlpha:1.0f];
                             [landmarkViewText setAlpha:1.0f];
                             [landmarkShadowLayer setAlpha:1.0f];
                             [landmarkViewHeader setAlpha:1.0f];
                             [landmarkImageView setAlpha:0.3f];
                             [attribLabel setAlpha:0.0f];
                             [landmarkImageButton setSelected:NO];
                         } 
                         completion:nil];
        
    }
}


- (IBAction)toggleFavourite:(id)sender
{
    if (![favouritesButton isSelected])
    {
        [favouritesButton setSelected:YES];
        
        NSString *statement = [self determineSettings];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:statement];
        [mapView removeAnnotations:[mapView annotations]];
        [mapView addAnnotations:[landmarks filteredArrayUsingPredicate:predicate]];
    }
    else 
    {
        [favouritesButton setSelected:NO];

        NSString *statement = [self determineSettings];
        
        if ([statement isEqualToString:@"all"])
        {
            [mapView removeAnnotations:[mapView annotations]];
            [mapView addAnnotations:landmarks];
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:statement];
            [mapView removeAnnotations:[mapView annotations]];
            [mapView addAnnotations:[landmarks filteredArrayUsingPredicate:predicate]];
        }
    }
    
    NSLog(@"%@", currPredicateStatement);

}

@end
