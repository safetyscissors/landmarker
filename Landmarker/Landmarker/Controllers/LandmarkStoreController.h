//
//  LandmarkStoreController.h
//  Landmarker
//
//  Created by Jason Lagaac on 26/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <sqlite3.h>

@class Landmark;

@interface LandmarkStoreController : NSObject
{
    NSString                        *dbPath;
}

- (NSArray *)getAllLandmarks;
- (void)updateLandmarkFavourte:(Landmark *)l;
@end
