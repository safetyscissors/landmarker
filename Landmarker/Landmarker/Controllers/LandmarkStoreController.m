//
//  LandmarkStoreController.m
//  Landmarker
//
//  Created by Jason Lagaac on 26/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LandmarkStoreController.h"
#import "Landmark.h"

@interface LandmarkStoreController (PrivateMethods)
- (void)checkAndCreateDB;
- (NSArray *)getLandmarksWithStatement:(NSString *)s;
@end

@implementation LandmarkStoreController

- (id)init
{
    if (self = [super init]) 
    {
        [self checkAndCreateDB];
    }
    
    return self;
}

- (void)dealloc
{
    [dbPath release];
    dbPath = nil;
}
    
#pragma mark - Database Creation
////////////////////////////////////////////////////////////////////////////////
// Initialise the database, and copy if necessary.
- (void)checkAndCreateDB
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    dbPath = [[documentsDir stringByAppendingFormat:@"/landmarks.sqlite"] retain];
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    NSError *error;
    
    if (!success) {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"/landmarks.sqlite"];
        NSLog(@"--> %@", defaultDBPath);
        BOOL copy_success = [fileManager copyItemAtPath:defaultDBPath 
                                                 toPath:dbPath 
                                                  error:&error];
        
        if (!copy_success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}




#pragma mark - Statement structure functions
///////////////////////////////////////////////////////////////////////////////

- (NSArray *)getAllLandmarks
{
    NSString *statement = [[NSString alloc] initWithFormat:@"select * FROM landmarks"];
    
    return [self getLandmarksWithStatement:statement];
    
}

#pragma mark - Database access functions
///////////////////////////////////////////////////////////////////////////////

// Execute the landmark statement
- (NSArray *)getLandmarksWithStatement:(NSString *)s
{
    NSMutableArray *dataPoints = [[NSMutableArray alloc] init];
    sqlite3 *database;
    
    NSLog(@"%@", dbPath);
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        sqlite3_create_function(database, "distance", 4, SQLITE_ANY, NULL, NULL, NULL, NULL);
        
        // Statment to obtain landmarks within a 1km radius
        NSString *statement = [[NSString alloc] initWithString:s];
        
        NSLog(@"%@", statement);
        
        const char *sqlStatement = [statement UTF8String];
        sqlite3_stmt *compiledStatement;
        
        // Execute SQL statement
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                // Read the data from the result row
                // Get values for table fields
                int dbID = [[NSNumber numberWithInt:(int)sqlite3_column_int(compiledStatement, 0)] intValue];
                NSMutableString *bName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSMutableString *bInfo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                int bBuilt = [[NSNumber numberWithInt:(int)sqlite3_column_int(compiledStatement, 3)] intValue];
                double bLatitude = [[NSNumber numberWithDouble:(double)sqlite3_column_double(compiledStatement, 5)] doubleValue];
                double bLongitude = [[NSNumber numberWithDouble:(double)sqlite3_column_double(compiledStatement, 6)] doubleValue];
                
                BOOL  bFavourite = [[NSNumber numberWithBool:(BOOL)sqlite3_column_int(compiledStatement, 7)] boolValue];
                int type = [[NSNumber numberWithInt:(int)sqlite3_column_int(compiledStatement, 8)] intValue];
                
                
                char *owner_tmp = (char *)sqlite3_column_text(compiledStatement, 9);
                NSMutableString *owner = nil;
                if (owner_tmp != nil)
                {
                  owner = [NSMutableString stringWithFormat:@"%s",owner_tmp];
                }
                
                CLLocationCoordinate2D pointCoord = CLLocationCoordinate2DMake(bLatitude, bLongitude);
                
                // Allocate all values to a new landmark type
                Landmark *landmarkPoint = [[Landmark alloc] initWithName:bName 
                                                                    info:bInfo 
                                                              coordinate:pointCoord 
                                                                   built:bBuilt 
                                                               imagePath:nil
                                                               favourite:bFavourite
                                                                    dbID:dbID
                                                                    type:type
                                                                   owner:owner];
                [dataPoints addObject:landmarkPoint];
            }
        }
    }
    sqlite3_close(database);
    
    return dataPoints;
}



// Update the landmark favourite value
- (void)updateLandmarkFavourte:(Landmark *)l
{
    Landmark *tmpLandmark = l;
    sqlite3 *database;
    
    NSLog(@"%@", dbPath);
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {    
        // Statment to obtain landmarks within a 1km radius
        NSString *statement = [[NSString alloc] initWithString:[NSString stringWithFormat:@"UPDATE landmarks SET favourite=%d WHERE uid=%d;", [tmpLandmark favourite], [tmpLandmark dbID]]];
        
        NSLog(@"%@", statement);
        
        const char *sqlStatement = [statement UTF8String];
        sqlite3_stmt *compiledStatement;
        
        // Execute SQL statement
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                NSLog(@"Updated");
            } else {
                NSLog(@"Error");
            }
        }
        
        sqlite3_finalize(compiledStatement);
        sqlite3_close(database);
    }
}


@end
