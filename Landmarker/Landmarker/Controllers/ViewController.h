//
//  ViewController.h
//  Landmarker
//
//  Created by Jason Lagaac on 24/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

@class LandmarkStoreController;
@class Landmark;

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>
{
    CLLocationManager         *locationManager;
    MKMapView                 *mapView;
    
    LandmarkStoreController   *landmarkStoreController;
    Landmark                  *presentedLandmark;
    NSArray                   *landmarks;
    UIView                    *landmarkView;
    UILabel                   *landmarkViewHeader;
    UITextView                *landmarkViewText;
    UIButton                  *landmarkFavouriteButton;
    UIButton                  *landmarkImageButton;
    UIImageView               *landmarkDivider;
    UIView                    *landmarkShadowLayer;
    UIImageView               *landmarkImageView;
    UILabel                   *attribLabel;
    
    UIView                    *periodMenuView;
    UIView                    *categoryMenuView;
    
    UIButton                  *favouritesButton;
    UIButton                  *categoryButton;
    UIButton                  *periodButton;
    
    UIButton                  *commercialSettingButton;
    UIButton                  *civicSettingButton;
    UIButton                  *museumSettingButton;
    UIButton                  *religiousSettingButton;
    UIButton                  *residentialSettingButton;
    UIButton                  *otherSettingButton;
    
    UIButton                  *period1600Button;
    UIButton                  *period1700Button;
    UIButton                  *period1800Button;
    UIButton                  *period1900Button;
    
    NSString                  *currPredicateStatement;
    BOOL                      firstRun;
    
    CLLocationCoordinate2D    currLoc;
}

@property (nonatomic, retain) IBOutlet MKMapView * mapView;
@property (nonatomic, retain) IBOutlet UIView    * landmarkView;
@property (nonatomic, retain) IBOutlet UIView    * periodMenuView;
@property (nonatomic, retain) IBOutlet UIView    * categoryMenuView;

@property (nonatomic, retain) IBOutlet UIButton  * favouritesButton;
@property (nonatomic, retain) IBOutlet UIButton  * categoryButton;
@property (nonatomic, retain) IBOutlet UIButton  * periodButton;

@property (nonatomic, retain) IBOutlet UILabel    * landmarkViewHeader;
@property (nonatomic, retain) IBOutlet UITextView * landmarkViewText;
@property (nonatomic, retain) IBOutlet UIButton   * landmarkFavouriteButton;
@property (nonatomic, retain) IBOutlet UILabel    * attribLabel;

@property (nonatomic, retain) IBOutlet UIButton  * commercialSettingButton;
@property (nonatomic, retain) IBOutlet UIButton  * civicSettingButton;
@property (nonatomic, retain) IBOutlet UIButton  * museumSettingButton;
@property (nonatomic, retain) IBOutlet UIButton  * religiousSettingButton;
@property (nonatomic, retain) IBOutlet UIButton  * residentialSettingButton;
@property (nonatomic, retain) IBOutlet UIButton  * otherSettingButton;

@property (nonatomic, retain) IBOutlet UIButton  * period1600Button;
@property (nonatomic, retain) IBOutlet UIButton  * period1700Button;
@property (nonatomic, retain) IBOutlet UIButton  * period1800Button;
@property (nonatomic, retain) IBOutlet UIButton  * period1900Button;

@property (nonatomic, retain) IBOutlet UIImageView * landmarkDivider;
@property (nonatomic, retain) IBOutlet UIView      * landmarkShadowLayer;
@property (nonatomic, retain) IBOutlet UIButton    * landmarkImageButton;
@property (nonatomic, retain) IBOutlet UIImageView * landmarkImageView;



- (IBAction)closeLandmarkView:(id)sender;
- (IBAction)presentPeriodMenu:(id)sender;
- (IBAction)presentCategoryMenu:(id)sender;
- (IBAction)toggleButton:(id)sender;
- (IBAction)toggleImageView:(id)sender;
- (IBAction)toggleFavourite:(id)sender;

- (void)presentLandmarkView;

@end
