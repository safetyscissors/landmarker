/*----------------------------------------------------------------------------
 *  Landmark.m
 *
 *  Created by Jason Lagaac on 6/2/2012
 *  Copyright (c) 2012. All rights reserved
 *
 *  Description: 
 *
 -----------------------------------------------------------------------------*/

#import "Landmark.h"


@implementation Landmark
@synthesize title, coordinate, info, imageName, favourite, builtDate, dbID, type, owner;

#pragma mark - Initialisation functions
///////////////////////////////////////////////////////////////////////////////
- (id)init
{
  if (self = [super init]) {
    
  }
  
  return self;
}

- (id)initWithName:(NSString *)nam
              info:(NSString *)inf
        coordinate:(CLLocationCoordinate2D)crd
             built:(int)bld
         imagePath:(NSString *)img
         favourite:(BOOL)fav
              dbID:(int)entryid
              type:(int)catType
             owner:(NSString *)o
{
  
  if (self = [self init]) {
    [self setTitle:nam];
    info = [[NSString alloc] initWithString: inf];
    coordinate = crd;
    builtDate = bld;
    //imagePath = [[NSString alloc] initWithString: img];
    favourite = fav;
    dbID = entryid;
    type = catType;
    owner = o;
      
    [owner retain];
    NSLog(@"Built: %@", owner);
  }
  
  return self;
}

#pragma mark - Deallocation
///////////////////////////////////////////////////////////////////////////////
- (void)dealloc 
{
  [title release];
  [info release];
  [imageName release];
  [imagePath release];
  
  title = nil;
  info = nil;
  imageName = nil;  
  imagePath = nil;
  
  [super dealloc];
}

# pragma mark - Toggle Functions
///////////////////////////////////////////////////////////////////////////////
- (void)toggleFavourite
{
  if (favourite)
    favourite = NO;
  else 
    favourite = YES;
}

@end
