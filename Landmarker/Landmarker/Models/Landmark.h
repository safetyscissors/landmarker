/*----------------------------------------------------------------------------
 *  Landmark.h
 *
 *  Created by Jason Lagaac on 6/2/2012
 *  Copyright (c) 2012. All rights reserved
 *
 *  Description: 
 *
 -----------------------------------------------------------------------------*/

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface Landmark : NSObject <MKAnnotation>
{
    NSString                  *title;
    NSString                  *info;
    NSString                  *imageName;
    int                       builtDate;
    NSString                  *imagePath;
    NSString                  *owner;
    
    CLLocationCoordinate2D    coordinate;
    int                       type;
    BOOL                      favourite;
    int                       dbID;
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSString *info;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSString *owner;
@property int builtDate;
@property int type;



@property BOOL  favourite;
@property int   dbID;                    

- (id)initWithName:(NSString *)nam
              info:(NSString *)inf
        coordinate:(CLLocationCoordinate2D)crd
             built:(int)bld
         imagePath:(NSString *)img
         favourite:(BOOL)fav
              dbID:(int)entryid
              type:(int)catType
             owner:(NSString *)o;

- (void)toggleFavourite;

@end
